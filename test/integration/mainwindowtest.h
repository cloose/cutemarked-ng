#ifndef MAINWINDOWTEST_H
#define MAINWINDOWTEST_H

#include <QObject>

class MainWindowTest : public QObject
{
    Q_OBJECT

private slots:
    void closesMainWindowOnFileExitTriggered();
    void hasTextEditor();
};

#endif // MAINWINDOWTEST_H
