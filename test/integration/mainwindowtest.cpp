#include "mainwindowtest.h"

#include <memory>
#include <QAction>
#include <QtTest>
#include <QPlainTextEdit>
#include "mainwindow.h"

void MainWindowTest::closesMainWindowOnFileExitTriggered()
{
    MainWindow mainWindow;
    QSignalSpy spy(qApp, &QApplication::lastWindowClosed);
    QAction *actionExit = mainWindow.findChild<QAction*>("actionExit");
    QVERIFY(actionExit != nullptr);

    mainWindow.show();
    QVERIFY(QTest::qWaitForWindowActive(&mainWindow));

    QTimer::singleShot(0, actionExit, &QAction::trigger);
    qApp->exec();

    QCOMPARE(spy.count(), 1);
}

void MainWindowTest::hasTextEditor()
{
    MainWindow mainWindow;
    QPlainTextEdit *editor = mainWindow.findChild<QPlainTextEdit*>("textEditor");
    QVERIFY(editor != nullptr);
}
