### integration test ###

aux_source_directory(. SRC_LIST)

include_directories(${CMAKE_SOURCE_DIR}/src/ui-lib)

add_executable(integrationtest ${SRC_LIST})
target_link_libraries(integrationtest ui-lib Qt5::Widgets Qt5::Test)

add_test(NAME integrationtest COMMAND integrationtest)
