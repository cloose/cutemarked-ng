#include <QApplication>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationDisplayName("CuteMarkEd NG");

    MainWindow win;
    win.show();

    return app.exec();
}
